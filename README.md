# go-wasm

From:
https://golangbot.com/webassembly-using-go/


## Locally

```
env GOOS=js GOARCH=wasm go build -o public/main.wasm wasm/main.go
go run server.go
```

Surf to `localhost:9090`.
