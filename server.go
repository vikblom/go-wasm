package main

import (
	"fmt"
	"log"
	"net/http"
)

func logMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("%s %s\n", r.Method, r.URL.Path)
		next.ServeHTTP(w, r)
	})
}

func main() {
	fs := http.FileServer(http.Dir("./public"))
	err := http.ListenAndServe(":9090", logMiddleware(fs))
	if err != nil {
		log.Fatal("Failed to start server", err)
	}
}
